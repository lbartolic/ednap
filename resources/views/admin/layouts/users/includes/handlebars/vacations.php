<script id="user-vacations-hbt" type="text/x-handlebars-template">
    <div class="table-responsive">
        <table class="table table-hover table-hover">
            <thead>
                <tr>
                    <th>Od</th>
                    <th>Do</th>
                    <th>Trajanje</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="chkins-tbl-body">
                {{#each data}}
                <tr class="_new-chkin-tr" data-user="1">
                    <td>{{date_format_1 date_from}}</td>
                    <td>{{date_format_1 date_to}}</td>
                    <td>{{date_difference_days date_from date_to}} dana</td>
                    <td class="_nowrap _text-right">
                        <button class="btn btn-danger btn-xs _delete-absence" data-absence="{{id}}"><span class="fa fa-remove _fa-reset"></span></button>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </div>
</script>