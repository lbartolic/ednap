<fieldset>
	<div class="row">
		<div class="col-sm-8">
			<div class="form-group">
				<label class="col-sm-2 control-label">Ime</label>
				<div class="col-sm-10">
					{{ Form::text('first_name', old('first_name'), ['placeholder' => 'ime zaposlenika', 'class' => 'form-control']) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Prezime</label>
				<div class="col-sm-10">
					{{ Form::text('last_name', old('last_name'), ['placeholder' => 'ime zaposlenika', 'class' => 'form-control']) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					{{ Form::text('email', old('email'), ['placeholder' => 'email adresa zaposlenika', 'class' => 'form-control']) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Lozinka</label>
				<div class="col-sm-10">
					@if(isset($user))
					{{ Form::text('password', '', ['placeholder' => 'lozinka zaposlenika', 'class' => 'form-control']) }}
					<span><small>Ostaviti prazno ako se ne mijenja</small></span>
					@else
					{{ Form::text('password', str_random(8), ['placeholder' => 'lozinka zaposlenika', 'class' => 'form-control']) }}
					@endif
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<div class="col-xs-12">
					<label class="control-labe _marginBtm6">Administrator</label>
					<div class="radio">
						<label>
							{{ Form::radio('is_admin', '0', true) }}
							Ne
						</label>
					</div>
					<div class="radio">
						<label>
							{{ Form::radio('is_admin', '1') }}
							Da
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<label class="control-label _marginBtm6">Sektori</label>
                    {!! Form::select('sectors[]', 
                    $sectors, $selected_sectors, array('multiple' => true, 'class' => 'form-control _select')) !!}
                </div>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group clearfix">
				<div class="col-xs-12">
                    <div class="pull-right">
						<button type="submit" class="btn btn-primary">Spremi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>

@section('js-after')

<script>
	$(document).ready(function() {
		$('._select').select2();
	});
</script>

@endsection