@extends('admin.layouts.master')

@section('main')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<h1 class="_title-main _v-center-items">
				Uredi podatke o zaposleniku
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9">
			{!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.user.update', $user->id], 'class' => 'form-horizontal']) !!}
				<div class="well">
					@include('admin.layouts.users.includes.partials.form')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection

@section('js-after')

<script>
	$(document).ready(function() {

	});
</script>

@endsection