@extends('admin.layouts.master')

@section('main')

<div class="container _user-show">
	<h1>
		{{ $user->full_name }}
		<span class="label label-info _marginLeft6" style="font-size: 12px; vertical-align: middle">
			@if($user->is_admin)
			Administrator
			@else
			Zaposlenik
			@endif
		</span>
	</h1>
	<div class="row">
		<div class="col-sm-7">
			<div class="well">
				<h3 class="clearfix">
					Općeniti podaci
					<a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-default btn-xs pull-right">Uredi</a>
				</h3>
				<div class="row">
					<div class="col-sm-6">
						<dl>
							<dt>Email adresa</dt>
							<dd>{{ $user->email }}</dd>
							<dt>Sektori</dt>
							<dd>
								@foreach($user->sectors as $sector)
								<span class="label label-default">{{ $sector->abbrevation }}</span>
								@endforeach
							</dd>
						</dl>
					</div>
					<div class="col-sm-6">
						<dl>
							<dt>Kreiran</dt>
							<dd>{{ $user->created_at->format('d.m.Y') }}</dd>
							<dt>Ažuriran</dt>
							<dd>{{ $user->updated_at->format('d.m.Y') }}</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-offset-2 col-md-3 col-sm-offset-0 col-sm-5">
			<div class="well">
				<div class="row">
					<div class="col-xs-12 _counter-holder">
						<span class="_counter-title">Sati rada</span>
						<span class="_counter-num">{{ $user->total_working_time_hi or '0'}}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row _v-center-items">
			            <div class="col-sm-6 col-xs-12">
			                <h3 class="panel-title clearfix">
			                    Prijave
			                    <span class="label label-primary _marginLeft6" id="user-check-ins-total"></span>
			                </h3>
			            </div>
			            <div class="col-sm-6 col-xs-12">
			                <div class="form-inline clearfix pull-right">
			                    <div class="form-group" id="user-check-ins-pag" data-user="{{ $user->id }}">
			                        <div class="btn-group" role="group">
			                            <button type="button" class="_pag__first btn btn-sm btn-default"><i class="fa fa-angle-double-left _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__prev btn btn-sm btn-default"><i class="fa fa-angle-left _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__next btn btn-sm btn-default"><i class="fa fa-angle-right _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__last btn btn-sm btn-default"><i class="fa fa-angle-double-right _fa-reset _fa-bigger"></i></button>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
				<div class="panel-body">
					@include('admin.layouts.users.includes.handlebars.check-ins')
					<div id="user-check-ins-holder"></div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading _no-padding">
					<div class="row _v-center-items">
						<div class="col-sm-8">
							<ul class="nav nav-tabs nav-justified">
								<li role="presentation" class="active">
									<a href="" data-toggle="tab" onclick="userAbsenceTypeChanged('sick_leaves')">Bolovanja</a>
								</li>
								<li role="presentation" class="">
									<a href="" data-toggle="tab" onclick="userAbsenceTypeChanged('vacations')">Godišnji</a>
								</li>
							</ul>
						</div>
						<div class="col-sm-4">
							<div class="form-inline clearfix pull-right" style="padding-right: 15px;">
			                    <div class="form-group" id="user-absences-pag" data-absence-type="sick_leaves" data-user="{{ $user->id }}">
			                        <div class="btn-group" role="group">
			                            <button type="button" class="_pag__first btn btn-sm btn-default"><i class="fa fa-angle-double-left _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__prev btn btn-sm btn-default"><i class="fa fa-angle-left _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__next btn btn-sm btn-default"><i class="fa fa-angle-right _fa-reset _fa-bigger"></i></button>
			                            <button type="button" class="_pag__last btn btn-sm btn-default"><i class="fa fa-angle-double-right _fa-reset _fa-bigger"></i></button>
			                        </div>
			                    </div>
			                </div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					@include('admin.layouts.users.includes.handlebars.vacations')
					<div id="user-absences-holder"></div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-after')

<script>
	$(document).ready(function() {
		paginateAjaxForTemplate(null, null, 'user-check-ins-pag', 'user-check-ins-hbt', 'user-check-ins-holder');
		paginateAjaxForTemplate(null, null, 'user-absences-pag', 'user-vacations-hbt', 'user-absences-holder');
	});
</script>

@endsection