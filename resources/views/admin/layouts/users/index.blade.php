@extends('admin.layouts.master')

@section('main')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<h1 class="_title-main _v-center-items">
				Pregled zaposlenika
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div id="curr-chk-ins__holder" class="panel panel-default">
				<div class="panel-heading _no-padding">
					<div class="row _v-center-items">
			            <div class="col-sm-9 col-xs-12">
							<ul class="nav nav-tabs nav-justified">
								<li role="presentation" class="{{ (!isSetGetParam('absence')) ? 'active' : '' }}">
									<a href="{{ route('admin.user.index') }}">Svi <span class="label label-primary _marginLeft6">{{ $counts['total'] }}</span></a>
								</li>
								<li role="presentation" class="{{ isActiveGetParam('absence', 'vacations') }}">
									<a href="{{ route('admin.user.index', ['absence' => 'vacations']) }}">Na godišnjem <span class="label label-primary _marginLeft6">{{ $counts['vacations'] }}</span></a>
								</li>
								<li role="presentation" class="{{ isActiveGetParam('absence', 'sick-leaves') }}">
									<a href="{{ route('admin.user.index', ['absence' => 'sick-leaves']) }}">Na bolovanju <span class="label label-primary _marginLeft6">{{ $counts['sick_leaves'] }}</span></a>
								</li>
							</ul>
						</div>
						<div class="col-sm-3 col-xs-12 clearfix" style="padding-right: 30px;">
							<div class="btn-group pull-right">
								<div class="btn-group">
									<a href="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="fa fa-sort-amount-desc"></span>
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu pull-right">
										<li class="{{ (!isSetGetParam('order_by')) ? 'active' : isActiveGetParam('order_by', 'last_name_asc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'last_name_asc')) }}">
												<span class="fa fa-sort-alpha-asc"></span>Prezime A-Ž
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'last_name_desc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'last_name_desc')) }}">
												<span class="fa fa-sort-alpha-desc"></span>Prezime Ž-A
											</a>
										</li>
										@if (isset($requestParams['absence']))
										@else
										<li class="{{ isActiveGetParam('order_by', 'created_at_desc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'created_at_desc')) }}">
												<span class="fa fa-calendar-plus-o"></span>Najnovije kreirani
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'created_at_asc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'created_at_asc')) }}">
												<span class="fa fa-calendar"></span>Najstarije kreirani
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'working_time_asc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'working_time_asc')) }}">
												<span class="fa fa-hourglass-start"></span>Najmanje vrijeme rada
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'working_time_desc') }}">
											<a href="{{ route('admin.user.index', arraySet($requestParams, 'order_by', 'working_time_desc')) }}">
												<span class="fa fa-hourglass-end"></span>Najveće vrijeme rada
											</a>
										</li>
										@endif
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			    <div class="panel-body">
			    	<div class="row _marginBtm15">
				    	<div class="col-xs-12">
							{!! Form::open(['method' => 'GET', 'url' => Request::url()]) !!}
							<div class="form-group" style="margin: 0;">
								<div class="input-group">
									<input type="text" class="form-control input-sm" placeholder="pretraži zaposlenike" name="search" value="{{ $searchTerm or '' }}">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-sm btn-default" type="button">
											<i class="fa _fa-reset fa-search"></i>
										</button>
									</span>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
			    	<div class="row">
				    	<div class="col-xs-12">
					        <div class="table-responsive">
								<table class="table table-hover table-hover">
									<thead>
										<tr>
											<th>ID</th>
											<th>Zaposlenik</th>
											<th>Email</th>
											@if (isset($requestParams['absence']))
											<th>Od</th>
											<th>Do</th>
											@else
											<th>Sektori</th>
											<th>Vrijeme rada</th>
											@endif
											<th></th>
										</tr>
									</thead>
									<tbody>
										@foreach($users as $user)
										<tr>
											<td>{{ $user->id }}</td>
											<td>{{ $user->full_name }}</td>
											<td>{{ $user->email }}</td>
											@if (isset($requestParams['absence']))
												@if ($requestParams['absence'] == 'vacations')
												<td>{{ $user->current_vacation->date_from->format('d.m.Y.') }} <small>({{$user->current_vacation->date_from_diff_days}} dana)</small></td>
												<td>{{ $user->current_vacation->date_to->format('d.m.Y.') }}</td>
												@elseif ($requestParams['absence'] == 'sick-leaves')
												<td>{{ $user->current_sick_leave->date_from->format('d.m.Y.') }} <small>({{$user->current_sick_leave->date_from_diff_days}} dana)</small></td>
												<td>{{ $user->current_sick_leave->date_to->format('d.m.Y.') }}</td>
												@endif
											@else
											<td>
												@foreach($user->sectors as $sector)
													<span class="label label-default">{{ $sector->abbrevation }}</span>
												@endforeach
											</td>
											<td>{{ $user->total_working_time_hi }}</td>
											@endif
											<td class="_nowrap _text-right">
												@if (isset($requestParams['absence']))
													
												@else
												<a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-primary btn-xs">Uredi</button>
												<a href="{{route('admin.user.show', $user->id)}}" class="_marginLeft6 btn btn-info btn-xs"><span class="fa fa-user _fa-reset"></span></a>
												<div style="display: inline-block;">
													{!! Form::open(['route' => ['admin.user.destroy', $user->id], 'method' => 'DELETE']) !!}
														<a class="btn btn-danger btn-xs" onclick="confirmToDelete(this)" style="{{ ($user->id != Auth::user()->id) ? 'display: inline-block' : 'visibility: hidden' }}">
															<span class="fa fa-remove _fa-reset"></span>
														</a>
													{!! Form::close() !!}
												</div>
												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="pull-right clearfix _rendered-pagin">
								{!! $users->appends($requestParams)->render() !!}
							</div>
						</div>
					</div>
			    </div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title clearfix">
			                    Odsutnosti
			                </h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => ['admin.absence.store']]) !!}
								<fieldset>
									<div class="form-group">
					                    <label class="control-label">Vrsta</label>
					                    	<div class="row">
						                    	<div class="col-xs-6">
													<div class="radio">
														<label>
															<input type="radio" name="absence_type" value="sick_leave" checked>
															Bolovanje
														</label>
													</div>
												</div>
												<div class="col-xs-6">
													<div class="radio">
														<label>
															<input type="radio" name="absence_type" value="vacation">
															Godišnji
														</label>
													</div>
												</div>
											</div>
					                    </div>
									<div class="form-group">
					                    <label class="control-label">Od (datum)</label>
										<div class='input-group' id="absence-date-from">
											<input type="text" class="form-control _date" placeholder="od" name="date_from">
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
									</div>
									<div class="form-group">
					                    <label class="control-label">Do (datum)</label>
										<div class='input-group' id="absence-date-to">
											<input type="text" class="form-control _date" placeholder="do" name="date_to">
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
									</div>
									<div class="form-group">
										<label class="control-label">Zaposlenici</label>
										<select class="form-control _select" name="user">
											@foreach($allUsers as $user)
											<option value="{{ $user->id }}">{{ $user->full_name }}</option>
											@endforeach
					                    </select>
				                    </div>
									<div class="clearfix">
					                    <div class="pull-right">
											<button type="submit" class="btn btn-primary">Spremi</button>
					                    </div>
					                </div>
								</fieldset>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-after')

<script>
	$(document).ready(function() {
		$('#absence-date-to input').datetimepicker({
			format: 'DD.MM.YYYY.'
		});
		$('#absence-date-from input').datetimepicker({
			format: 'DD.MM.YYYY.'
		});
		$('._select').select2();
	});
</script>

@endsection