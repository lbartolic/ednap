<fieldset>
	<div class="form-group">
		{{ Form::text('name', old('name'), ['placeholder' => 'naziv sektora', 'class' => 'form-control']) }}
	</div>
	<div class="form-group">
		{{ Form::text('abbrevation', old('abbrevation'), ['placeholder' => 'kratica sektora', 'class' => 'form-control']) }}
	</div>
	<div class="form-group">
		<label class="control-label _marginBtm6">Zaposlenici</label>
        {!! Form::select('users[]', $users, $selected_users, array('multiple' => true, 'class' => 'form-control _select')) !!}
    </div>
    <div class="alert alert-info">
		Odaberi zaposlenike ukoliko trebaju pripadati sektoru koji se kreira.
	</div>
	<div class="clearfix">
        <div class="pull-right">
			<button type="submit" class="btn btn-primary">Spremi</button>
        </div>
    </div>
</fieldset>

@section('js-after')

<script>
	$(document).ready(function() {
		$('._select').select2();
	});
</script>

@endsection