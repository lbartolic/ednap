@extends('admin.layouts.master')

@section('main')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<h1 class="_title-main _v-center-items">
				Uredi podatke o sektoru
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			{!! Form::model($sector, ['method' => 'PUT', 'route' => ['admin.sector.update', $sector->id]]) !!}
				<div class="well">
					@include('admin.layouts.sectors.includes.partials.form')
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection

@section('js-after')

<script>
	$(document).ready(function() {

	});
</script>

@endsection