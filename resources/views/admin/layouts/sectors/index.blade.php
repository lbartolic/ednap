@extends('admin.layouts.master')

@section('main')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<h1 class="_title-main">
				Pregled sektora
				<span class="badge _marginLeft6">ukupno: <strong>{{ $counts['total'] }}</strong></span>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div id="curr-chk-ins__holder" class="panel panel-default">
				<div class="panel-heading">
					<div class="row _v-center-items">
						<div class="col-sm-6 col-xs-8">
							{!! Form::open(['method' => 'GET', 'url' => Request::url()]) !!}
							<div class="form-group" style="margin: 0;">
								<div class="input-group">
									<input type="text" class="form-control input-sm" placeholder="pretraži sektore" name="search" value="{{ $searchTerm or '' }}">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-sm btn-default" type="button">
											<i class="fa _fa-reset fa-search"></i>
										</button>
									</span>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
						<div class="col-sm-6 col-xs-4 clearfix">
							<div class="btn-group pull-right">
								<div class="btn-group">
									<a href="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="fa fa-sort-amount-desc"></span>
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu pull-right">
										<li class="{{ (!isSetGetParam('order_by')) ? 'active' : isActiveGetParam('order_by', 'name_asc') }}">
											<a href="{{ route('admin.sector.index', arraySet($requestParams, 'order_by', 'name_asc')) }}">
												<span class="fa fa-sort-alpha-asc"></span>Naziv A-Ž
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'name_desc') }}">
											<a href="{{ route('admin.sector.index', arraySet($requestParams, 'order_by', 'name_desc')) }}">
												<span class="fa fa-sort-alpha-desc"></span>Naziv Ž-A
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'emp_count_asc') }}">
											<a href="{{ route('admin.sector.index', arraySet($requestParams, 'order_by', 'emp_count_asc')) }}">
												<span class="fa fa-sort-alpha-desc"></span>Najmanja zastupljenost
											</a>
										</li>
										<li class="{{ isActiveGetParam('order_by', 'emp_count_desc') }}">
											<a href="{{ route('admin.sector.index', arraySet($requestParams, 'order_by', 'emp_count_desc')) }}">
												<span class="fa fa-sort-alpha-desc"></span>Najveća zastupljenost
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			    <div class="panel-body">
			        <div class="table-responsive">
						<table class="table table-hover table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Naziv</th>
									<th>Kratica</th>
									<th>#</th>
									<th>Kreiran</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($sectors as $sector)
								<tr>
									<td>{{ $sector->id }}</td>
									<td>{{ $sector->name }}</td>
									<td>{{ $sector->abbrevation }}</td>
									<td>
										<span class="label label-primary">{{ $sector->users_count }}</span>
									</td>
									<td>{{ $sector->created_at->format('d.m.Y.') }}</td>
									<td class="_nowrap _text-right">
										<a href="{{ route('admin.sector.edit', $sector->id) }}" class="btn btn-primary btn-xs">
											Uredi
										</a>
										<div style="display: inline-block;">
											{!! Form::open(['route' => ['admin.sector.destroy', $sector->id], 'method' => 'DELETE']) !!}
												<a class="btn btn-danger btn-xs _delete-conf" onclick="confirmToDelete(this)"><span class="fa fa-remove _fa-reset"></span></a>
											{!! Form::close() !!}
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="pull-right clearfix _rendered-pagin">
						{!! $sectors->appends($requestParams)->render() !!}
					</div>
			    </div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title clearfix">
			                    Novi sektor
			                </h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => ['admin.sector.store']]) !!}
								@include('admin.layouts.sectors.includes.partials.form')
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-after')
<script>

</script>
    @endsection