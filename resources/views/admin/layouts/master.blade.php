<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="icon" href="">
      <title>@yield('pageTitle') · Evidencija</title>
      
      {!! Html::style('https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext') !!}
      {!! Html::style('css/admin.css') !!}
   </head>

   <body class="@yield('bodyClass')" style="@yield('bodyStyle')">
      @if(Auth::user())
      @include('admin.includes.nav')
      @endif
      @yield('main')

      @yield('js-before')
      <script>var APP_URL = {!! json_encode(url('/')) !!};</script>
      {!! Html::script('https://cdn.socket.io/socket.io-1.4.5.js') !!}
      {!! Html::script('js/admin.js') !!}
      @yield('js-after')
   </body>
</html>