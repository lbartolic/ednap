<div class="modal fade" id="modal-checkins-date-picker" tabindex="-1" role="dialog" data-backdrop="static">
	<div class="modal-dialog" id="modal-edit-checkin-holder">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Odabir datuma evidencije</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
			        <div id="checkins-date-picker"></div>
			    </div>
			</div>
			<div class="modal-footer clearfix">
				<button href="#" class="btn btn-default" data-dismiss="modal">Odustani</button>
		    </div>
		</div>
	</div>
</div>