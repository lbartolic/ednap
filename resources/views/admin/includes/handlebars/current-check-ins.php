<script id="current-check-ins-hbt" type="text/x-handlebars-template">
    <div class="table-responsive">
        <table class="table table-hover table-hover">
            <thead>
                <tr>
                    <th>Zaposlenik</th>
                    <th>Sektor</th>
                    <th>Vrijeme prijave</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="chkins-tbl-body">
                {{#each check_ins}}
                    {{> new_check_in}}
                {{/each}}
            </tbody>
        </table>
    </div>
</script>