<script id="new-not-checked-in-hbt" type="text/x-handlebars-template">
    <tr data-user="{{ user.id }}">
        <td>{{ user.full_name }}</td>
        <td>
            {{#each user.sectors}}
            <span class="label label-default">{{ abbrevation }}</span>
            {{/each}}
        </td>
        <td class="_nowrap _text-right">
            <button class="btn btn-primary btn-xs">Uredi</button>
            <button class="btn btn-info btn-xs"><span class="fa fa-user _fa-reset"></span></button>
        </td>
    </tr>
</script>