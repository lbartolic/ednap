<script id="check-ins-custom-date-hbt" type="text/x-handlebars-template">
    <div class="table-responsive">
        <table class="table table-hover table-hover">
            <thead>
                <tr>
                    <th>Zaposlenik</th>
                    <th>Prijava</th>
                    <th>Odjava</th>
                    <th>Trajanje</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="chkins-tbl-body">
                {{#each data}}
                <tr class="_new-chkin-tr" data-user="1">
                    <td>{{user.full_name}}</td>
                    <td>{{check_in_local_time_format}}</td>
                    <td>{{check_out_local_time_format}}</td>
                    <td>{{check_in_diff}}</td>
                    <td class="_nowrap _text-right">
                        <button class="btn btn-danger btn-xs _delete-check-in" data-check-in="{{id}}"><span class="fa fa-remove _fa-reset"></span></button>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </div>
</script>