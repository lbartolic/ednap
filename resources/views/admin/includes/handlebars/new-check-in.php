<script id="new-check-in-hbt" type="text/x-handlebars-template">
	<tr class="_new-chkin-tr" data-user="{{ user.id }}">
        <td>{{ user.full_name }}</td>
        <td>
            {{#each user.sectors}}
            <span class="label label-default">{{ abbrevation }}</span>
            {{/each}}
        </td>
        <td>
            <span class="_marginRight6">{{ check_in_local_time_format }}</span> <span class="text-success"><small><b><span class="fa fa-clock-o _fa-reset"></span> <span class="_current-time-diff" data-check-in="{{ check_in }}">{{ time_difference check_in }}</span></b></small></span>
        </td>
        <td class="_nowrap _text-right">
            <button class="btn btn-primary btn-xs _edit-checkin" data-checkin="{{ id }}">Uredi</button>
            <a href="admin/user/{{user.id}}" class="btn btn-info btn-xs"><span class="fa fa-user _fa-reset"></span></a>
        </td>
    </tr>
</script>