<script id="modal-edit-checkin-hbt" type="text/x-handlebars-template">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">Uredi prijavu</h4>
		</div>
		<div class="modal-body">
			<div id="checkin-edit__holder">
				<div class="row">
					<div class="col-xs-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								Zaposlenik 
								<a href="admin/user/{{data.user.id}}" class="btn btn-xs btn-default pull-right _btn-show-user">Vidi više</a>
							</div>
							<div class="panel-body">
								<h5 class="_no-margin _marginBtm15">{{data.user.full_name}}</h5>
								<span class="_marginBtm6 text-muted">Email</span>
								<p class="_no-margin">{{data.user.email}}</p>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="panel panel-default">
							<div class="panel-heading">Vrijeme prijave</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-6">
										<span class="_marginBtm6 text-muted">Prijava</span>
										<h3 class="text-info _no-margin">{{time_format_1 data.check_in_local.date}}</h3>
									</div>
									<div class="col-xs-6">
										<span class="_marginBtm6 text-muted">Trajanje</span>
										<h3 class="text-success _no-margin">{{data.check_in_diff}}</h3>
									</div>
								</div>
								<span class="text-info _marginTop6">{{date_format_1 data.check_in}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer clearfix">
			<div class="row">
				<div class="col-sm-3">
					<button href="#" class="btn btn-danger pull-left _marginTop6 _delete-current-checkin" data-check-in="{{data.id}}">Obriši</button>
				</div>
				<div class="col-sm-9 clearfix">
					<button href="#" class="btn btn-warning _marginTop6 _stop-current-checkin" data-check-in="{{data.id}}">Završi</button>
					<button href="#" class="btn btn-default _marginTop6" data-dismiss="modal">Odustani</button>
				</div>
			</div>
	    </div>
	</div>
</script>