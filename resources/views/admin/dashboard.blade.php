@extends('admin.layouts.master')

@section('main')

@include('admin.includes.modal-edit-checkin')
@include('admin.includes.modal-date-picker')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<h1 class="_title-main _v-center-items">
				<span class="_marginRight15">Evidencija <span id="dashboard-date">{{ $now->format('d.m.Y.') }}</span></span> 
				<button class="btn btn-sm btn-default" onclick="openCheckInsDatePicker()"><span class="fa fa-calendar _fa-reset"></span></button>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="row" id="checkins-today-row">
				<div class="col-xs-12">
					<div id="curr-chk-ins__holder" class="panel panel-default">
						@include('admin.includes.handlebars.current-check-ins')
						@include('admin.includes.handlebars.new-check-in')
						<div class="panel-heading">
					        <div class="row _v-center-items">
					            <div class="col-sm-6 col-xs-12">
					                <h3 class="panel-title clearfix">
					                    Trenutno prijavljeni
					                    <span id="curr-chk-ins-num" class="label label-primary _marginLeft6"></span>
					                </h3>
					            </div>
					            <div class="col-sm-6 col-xs-12">
					                <div class="form-inline clearfix pull-right">
					                    <div class="form-group" id="chk-ins-pagin__holder" data-start="0">
					                        <!--<span id="pagin-info" class="_paginationInfo"><b id="pagin-info__fromNum">1</b><b> – </b><b id="pagin-info__toNum">4</b> od <b id="pagin-info__totalNum">20</b></span>-->
					                        <div class="btn-group" role="group">
					                            <button type="button" class="_pag__first btn btn-sm btn-default"><i class="fa fa-angle-double-left _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__prev btn btn-sm btn-default"><i class="fa fa-angle-left _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__next btn btn-sm btn-default"><i class="fa fa-angle-right _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__last btn btn-sm btn-default"><i class="fa fa-angle-double-right _fa-reset _fa-bigger"></i></button>
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					    <div class="alert alert-success" id="chk-ins-new-alert" style="display: none;">
					    	<b>Nova prijava!</b> Korisnik <b class="_chk-ins-alert-user"></b> upravo se prijavio.
					    </div>
						<div>
							<div id="chkd-in-sectors-occup" class="progress progress-striped active">
				                
				            </div>
						</div>
					    <div class="panel-body" id="curr-chk-ins__body">
					        
					    </div>
					</div>
				</div>
			</div>
			<div class="row" id="checkins-custom-date-row" style="display: none;">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row _v-center-items">
					            <div class="col-sm-6 col-xs-12">
					                <h3 class="panel-title clearfix">
					                    Prijave
					                    <span class="label label-primary _marginLeft6" id="date-check-ins-total"></span>
					                </h3>
					            </div>
					            <div class="col-sm-6 col-xs-12">
					                <div class="form-inline clearfix pull-right">
					                    <div class="form-group" id="date-check-ins-pag" data-date="">
					                        <div class="btn-group" role="group">
					                            <button type="button" class="_pag__first btn btn-sm btn-default"><i class="fa fa-angle-double-left _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__prev btn btn-sm btn-default"><i class="fa fa-angle-left _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__next btn btn-sm btn-default"><i class="fa fa-angle-right _fa-reset _fa-bigger"></i></button>
					                            <button type="button" class="_pag__last btn btn-sm btn-default"><i class="fa fa-angle-double-right _fa-reset _fa-bigger"></i></button>
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
						</div>
						<div class="panel-body">
							@include('admin.includes.handlebars.check-ins-custom-date')
							<div id="check-ins-date-holder"></div>
						</div>
					</div>
					<h4 style="font-weight: 300; margin: 15px 0 20px 0;">Prijave kroz dan</h4>
					<div id="dashboard-date-chart-holder">
						<canvas id="dashboard-date-chart" style="width: 100%; height: 300px"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title clearfix">
			                    Nova prijava
			                </h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => ['admin.check-in.store']]) !!}
								<fieldset>
									<div class="form-group">
					                    <label class="control-label">Vrsta prijave</label>
				                    	<div class="row">
					                    	<div class="col-xs-6">
												<div class="radio">
													<label>
														<input type="radio" name="checkin_type" value="current" checked onclick="newCheckInTypeChange(this)">
														Trenutna
													</label>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="radio">
													<label>
														<input type="radio" name="checkin_type" value="past" onclick="newCheckInTypeChange(this)">
														Prošla
													</label>
												</div>
											</div>
										</div>
					                </div>
									<div class="form-group">
										<div class='input-group' id='new-check-in--from'>
						                    <input type='text' class="form-control" placeholder="prijavljen od" name="check_in" value="{{ old('check_in') }}"/>
						                    <span class="input-group-addon">
						                        <span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                </div>
									</div>
									<div id="new-check-in--past" style="opacity: 0.4;">
										<div class="form-group">
											<div class='input-group' id='new-check-in--to'>
							                    <input type='number' min="1" step="0.5" class="form-control" placeholder="trajanje prijave (h)" name="check_in_duration" value="{{ old('check_in_duration') }}" disabled/>
							                    <span class="input-group-addon">
							                        <span>sati</span>
							                    </span>
						                    </div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label">Zaposlenici</label>
										<select class="form-control _select" name="user">
											@foreach($allUsers as $user)
											<option value="{{ $user->id }}">{{ $user->full_name }}</option>
											@endforeach
					                    </select>
				                    </div>
									<div class="clearfix">
					                    <div class="pull-right">
											<button type="submit" class="btn btn-primary">Spremi</button>
					                    </div>
					                </div>
					                <input type="hidden" value="Europe/Zagreb" name="tz">
								</fieldset>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-after')

<script>
	$(document).ready(function() {
		getCurrentCheckIns(function(data) {
			echoListenCheckIn();
			echoListenCheckOut();
			arrangeCurrentCheckIns(data);
			makeCurrentCheckIns();
			drawCurrentSectionsProgress(data);
			
			$('#new-check-in--from input').datetimepicker({
				format: 'HH:mm'
			});

			$('._select').select2();
		});
	});

	function newCheckInTypeChange(el) {
		console.log(el.value);
		if (el.value == 'current') {
			$('#new-check-in--past').css('opacity', 0.4).find('input').attr('disabled', true);
			$('#new-check-in--from input').data("DateTimePicker").options({
				format: 'HH:mm'
			});
		}
		else {
			$('#new-check-in--past').css('opacity', 1).find('input').attr('disabled', false);
			$('#new-check-in--from input').data("DateTimePicker").options({
				format: 'DD.MM.YYYY. HH:mm'
			});
		}
	}
</script>

@endsection