<!DOCTYPE html>
<html lang="en" class="_v-center-items" style="height: 100%;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        {!! Html::style('https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext') !!}
        <link href="/css/admin.css" rel="stylesheet">
    </head>
    <body style="width: 100%;">
        <div id="app">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <a href="{{ route('frontend.index') }}" class="btn btn-lg btn-info btn-block" style="padding: 40px 15px; margin: 10px 0;">Zaposlenici</a>
                    </div>
                    <div class="col-sm-6 text-center">
                    <a href="{{ route('admin.index') }}" class="btn btn-lg btn-primary btn-block" style="padding: 40px 15px; margin: 10px 0;">Administracija</a>
                    </div>
                </div>
                <div class="row" style="margin-top: 30px;">
                    <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                        {{ Form::open(['method' => 'POST', 'action' => 'Auth\LoginController@logout'])}}
                        <button type="submit" class="btn btn-default btn-block" style="padding: 20px 15px;">Odjava</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>