<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="icon" href="">
      <title>@yield('pageTitle') · Evidencija</title>
      
      {!! Html::style('https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext') !!}
      {!! Html::style('css/frontend.css') !!}
   </head>

   <body class="@yield('bodyClass')" style="@yield('bodyStyle')">
      @yield('main')

      @yield('js-before')
      {{--{!! Html::script('js/admin.js') !!}--}}
      @yield('js-after')
   </body>
</html>
<script>
  var APP_URL = {!! json_encode(url('/')) !!};
</script>