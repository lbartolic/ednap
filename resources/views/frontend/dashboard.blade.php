@extends('frontend.layouts.master')

@section('main')

<div class="container-fluid _marginTop15">
	<div class="row">
		<div class="col-xs-offset-2 col-xs-8">
			@if (!Auth::user()->is_checked_in)
			{{ Form::open(['method' => 'POST', 'route' => 'frontend.check-in.make'])}}
				<button class="btn btn-primary btn-lg btn-block" type="submit">Rad - PRIJAVA</button>
			{{ Form::close() }}
			@else
			{{ Form::open(['method' => 'POST', 'route' => ['frontend.check-in.check-out']])}}
				<button class="btn btn-danger btn-lg btn-block" type="submit">Rad - ODJAVA</button>
			{{ Form::close() }}
			@endif
		</div>
	</div>
</div>

@endsection