$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function showDefaultConfirmSwal(title, text, confirmText, cancelText, callback) {
	if (title == null || title == undefined) title = "Upozorenje";
	if (text == null || text == undefined) text = "Odabrana stavka bit će obrisana";
	if (confirmText == null || confirmText == undefined) confirmText = "OK";
	if (cancelText == null || cancelText == undefined) cancelText = "Odustani";
	swal({   
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: confirmText,
        cancelButtonText: cancelText
    }, 
    function() {   
    	callback();
    });
}

function confirmToDelete(el) {
    var $form = $(el).closest("form");
    showDefaultConfirmSwal(null, 'Odabrana stavka bit će obrisana.', null, null, function() {
        $form.submit();
    });
}

function setupLinkFormSubmit() {
	$(document).ready(function() {
	    $('._aFormSubmit').click(function(e) {
	    	e.preventDefault();
	    	$(this).closest('form').submit();
	    });
    });
}
setupLinkFormSubmit();


function showNotification(notifTitle, notifMsg, notifType, notifDelayInd) {
    var title = true,
        msg = '',
        type = 'success',
        delayIndicator = true;
            
    if (notifTitle !== undefined && notifTitle !== null) title = notifTitle;
    if (notifMsg !== undefined && notifMsg !== null) msg = notifMsg;
    if (notifType !== undefined && notifType !== null) type = notifType;
    if (notifDelayInd !== undefined && notifDelayInd !== null) delayIndicator = notifDelayInd;
    
    Lobibox.notify(type, {
        title: title,
        size: 'normal',
        icon: false,
        delayIndicator: delayIndicator,
        delay: 8000,
        rounded: false,
        position: 'center bottom',
        msg: msg,
        sound: false,
        showClass: 'fadeInUp',
        hideClass: 'fadeOutDown'
    });
}

/**
 * Initialize JS DOM actions on each page load.
 */
$('document').ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
});

function saveToLocalStorage(key, data) {
	var a = [];
    a = JSON.parse(localStorage.getItem(key));
    a.push(data);
    localStorage.setItem(key, JSON.stringify(a));
}

function removeFromLocalStorage(key, id) {
	// remove depending on object id property
	var a = [];
    a = JSON.parse(localStorage.getItem(key));
    a = _.filter(a, function(item) {
	    return item.id !== parseInt(id)
	});
    localStorage.setItem(key, JSON.stringify(a));
}

function getCheckInTimeDiff(val) {
	var d1 = moment.utc(val);
    var d2 = moment.utc();
    return moment(d2.diff(d1)).utc().format("H[h] mm'ss''");
}

function getCurrentCheckIns(callback) {
	$.get(APP_URL + '/admin/ajax/check-in/get-checked-in', function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function arrangeCurrentCheckIns(data) {
	if (data === null || data === undefined) {
		data = JSON.parse(localStorage.getItem("currentCheckInsJSON"));
	}
	data = (_.sortBy(data, function(obj) { return obj.check_in })).reverse();
	localStorage.setItem('currentCheckInsJSON', JSON.stringify(data));
}

function makeCurrentCheckIns(data, action, lastPos) {
	if (data === null || data === undefined) {
		data = JSON.parse(localStorage.getItem("currentCheckInsJSON"));
	}

	var numOfItems = 5;
	var start = 0;

	switch(action) {
		case 'first':
			start = 0;
			break;
		case 'last':
			var tempStart = parseInt(data.length/numOfItems)*numOfItems;
			start = (tempStart != data.length) ? tempStart : tempStart - numOfItems;
			break;
		case 'previous':
			(lastPos - numOfItems < 0) ? start = 0 : start = lastPos - numOfItems;
			break;
		case 'next':
			start = lastPos;
			break;
		case 'new':
			start = parseInt($('#chk-ins-pagin__holder').attr('data-start'));
			break;
		case 'removed':
			var dataStart = parseInt($('#chk-ins-pagin__holder').attr('data-start'));
			(dataStart >= data.length) ? start = dataStart - numOfItems : start = dataStart;
			break;
	}
	if (start < 0) start = 0;

	var to = start + numOfItems;
	if (to > data.length) to = data.length;
	var context = {'check_ins': data.slice(start, to)};
    var template = document.getElementById('current-check-ins-hbt').innerHTML;
    var compiled = Handlebars.compile(template)(context);
	$('#curr-chk-ins__body').html(compiled);
	$('#chk-ins-pagin__holder').attr('data-start', start);
	$('#curr-chk-ins-num').html(data.length);

	if (start == 0) {
		$('#chk-ins-pagin__holder ._pag__prev, #chk-ins-pagin__holder ._pag__first').attr('disabled', 'disabled');
	}
	else {
		$('#chk-ins-pagin__holder ._pag__prev, #chk-ins-pagin__holder ._pag__first').removeAttr('disabled');
	}
	if (to >= data.length) {
		$('#chk-ins-pagin__holder ._pag__next, #chk-ins-pagin__holder ._pag__last').attr('disabled', 'disabled');
	}
	else {
		$('#chk-ins-pagin__holder ._pag__next, #chk-ins-pagin__holder ._pag__last').removeAttr('disabled');
	}

	$('#chk-ins-pagin__holder ._pag__first').off().click(function() {
		makeCurrentCheckIns(null, 'first', start);
	});
	$('#chk-ins-pagin__holder ._pag__last').off().click(function() {
		makeCurrentCheckIns(null, 'last', start);
	});
	$('#chk-ins-pagin__holder ._pag__prev').off().click(function() {
		makeCurrentCheckIns(null, 'previous', start);
	});
	$('#chk-ins-pagin__holder ._pag__next').off().click(function() {
		makeCurrentCheckIns(null, 'next', to);
	});

	$('._edit-checkin').off('click').click(function(e) {
        e.preventDefault();
        showModalEditCheckIn($(this).attr('data-checkin'));
    });

	setInterval(function() {
		$('._current-time-diff').each(function() {
			var checkIn = $(this).attr('data-check-in');
			$(this).html(getCheckInTimeDiff(checkIn));
		});
	}, 1000);
}

function appendNewCheckIn(data) {
	saveToLocalStorage('currentCheckInsJSON', data.check_in);
	arrangeCurrentCheckIns();
    makeCurrentCheckIns(null, 'new', null);
    drawCurrentSectionsProgress();

    $('#chkins-tbl-body').find('[data-user="' + data.check_in.user.id + '"]')
    	.animate({'backgroundColor' : 'rgba(89, 214, 94, 0.2)'}, 300, 'easeOutExpo', function() {
    		$(this).animate({'backgroundColor' : 'none'}, 200);
	    });

    if (parseInt($('#chk-ins-pagin__holder').attr('data-start')) != 0) {
		showNewCheckInAlert(data);
	}
}

function removeCurrentCheckIn(data) {
	removeFromLocalStorage('currentCheckInsJSON', data.check_in.id);
	arrangeCurrentCheckIns();

    $('#chkins-tbl-body').find('[data-user="' + data.check_in.user.id + '"]')
		.css({'background' : 'rgba(214, 89, 89, 0.3)'})
		.delay(130)
		.fadeOut('fast')
		.promise()
		.done(function() {
			makeCurrentCheckIns(null, 'removed', null);
			drawCurrentSectionsProgress();
		});
}

function showNewCheckInAlert(data) {
	$('#chk-ins-new-alert ._chk-ins-alert-user').html(data.check_in.user.full_name);
	$('#chk-ins-new-alert').slideDown().delay(2000).slideUp();
}

function drawCurrentSectionsProgress(data) {
	if (data === null || data === undefined) {
		data = JSON.parse(localStorage.getItem("currentCheckInsJSON"));
	}
	var sectors = _.chain(data).pluck("user").flatten().pluck("sectors").flatten().countBy('abbrevation').value();
	var sectorKeys = _.keys(sectors);
	var sectorValues = _.values(sectors) 

	var maxCount = _.max(sectorValues);
	var totalCount = _.reduce(sectorValues, function(memo, num){ return memo + num; }, 0);
	var sectorsOccup = $('#chkd-in-sectors-occup');
	sectorsOccup.empty();
	for(var i = 0; i < sectorKeys.length; i++) {
		var width = (sectorValues[i]/totalCount)*100;
		var color = 'rgba(33, 150, 243, 0.5)';
		if ((i % 2 == 0) || (i == 0)) color = 'rgba(33, 150, 243, 1)';
		sectorsOccup.append('<div class="progress-bar _chkd-in-sector-progress" style="width: ' + width + '%; background-color: ' + color + '; position: relative;" data-toggle="tooltip" data-placement="bottom" title="' + sectorKeys[i] + ' : ' + sectorValues[i] + '"></div>');
	}
	$('._chkd-in-sector-progress').tooltip({
		animation: false,
		trigger: 'manual',
		template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner _tooltip-small"></div></div>'
	}).tooltip('show');

	$(window).bind('resize', function() {
        $('._chkd-in-sector-progress').tooltip('show');
    });
}

function showModalEditCheckIn(resId) {
    $.get(APP_URL + '/admin/ajax/check-in/' + resId + '/edit', function(r) {}, "json")
        .done(function(data) {
            $('#modal-edit-checkin').modal().show();
            var hbTemplate = document.getElementById('modal-edit-checkin-hbt').innerHTML;
			var context = {'data': data};
			var compiled = Handlebars.compile(hbTemplate)(context);
			$('#modal-edit-checkin-holder').html(compiled);
			$('._stop-current-checkin').off().click(function() {
				var id = $(this).attr('data-check-in');
				stopCurrentCheckIn(id, function() {
					$('#modal-edit-checkin').modal('hide');
				});
			});
			$('._delete-current-checkin').off().click(function() {
				var id = $(this).attr('data-check-in');
				deleteCheckIn(id, function() {
					$('#modal-edit-checkin').modal('hide');
				});
			})
        })
        .fail(function (data) {
        });
}

function userAbsenceTypeChanged(type) {
	$('#user-absences-pag').attr('data-absence-type', type);
	paginateAjaxForTemplate(null, null, 'user-absences-pag', 'user-vacations-hbt', 'user-absences-holder');
}

function getCheckInsForDatePaginated(date, page, callback) {
	$.get(APP_URL + '/admin/ajax/check-in/check-ins/' + date + '?page=' + page, function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function getCheckInsDateStats(date, callback) {
	$.get(APP_URL + '/admin/ajax/check-in/check-ins/' + date + '/stats', function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function getUsersCheckInsPaginated(user, page, callback) {
	$.get(APP_URL + '/admin/ajax/user/' + user + '/check-ins?page=' + page, function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function getUsersVacationsPaginated(user, page, callback) {
	$.get(APP_URL + '/admin/ajax/user/' + user + '/absence/vacations?page=' + page, function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function getUsersSickLeavesPaginated(user, page, callback) {
	$.get(APP_URL + '/admin/ajax/user/' + user + '/absence/sick-leaves?page=' + page, function(r) {}, "json")
        .done(function(data) {
            callback(data);
        })
        .fail(function (data) {
        });
}

function paginateAjaxForTemplate(data, page, paginator, template, holder) {
	if (page === null || page === undefined) page = 1;
	if (data === null || data === undefined) {
		if (paginator == 'date-check-ins-pag') {
			var date = $('#date-check-ins-pag').attr('data-date');
			getCheckInsForDatePaginated(date, page, function(data) {
				//$('#user-check-ins-total').html(data.total);
				paginateDataWithTemplate(data, page, paginator, template, holder);
				getCheckInsDateStats(date, function(data) {
					makeActivityChartByHour(data);
				});
			});
		}
		if (paginator == 'user-check-ins-pag') {
			var user = $('#user-check-ins-pag').attr('data-user');
			getUsersCheckInsPaginated(user, page, function(data) {
				$('#user-check-ins-total').html(data.total);
				paginateDataWithTemplate(data, page, paginator, template, holder);
			});
		}
		if (paginator == 'user-absences-pag') {
			var user = $('#user-absences-pag').attr('data-user');
			if ($('#user-absences-pag').attr('data-absence-type') == 'sick_leaves') {
				getUsersSickLeavesPaginated(user, page, function(data) {
					paginateDataWithTemplate(data, page, paginator, template, holder);
				});
			}
			else {
				getUsersVacationsPaginated(user, page, function(data) {
					paginateDataWithTemplate(data, page, paginator, template, holder);
				});
			}
		}
	}
}

function paginateDataWithTemplate(data, page, paginator, template, holder) {
	console.log(data, page, paginator, template, holder)
	var dataArr = data.data;
	var numOfItems = data.per_page;
	var to = data.to;
	var currentPage = data.current_page;
	var hbTemplate = document.getElementById(template).innerHTML;
	var context = {'data': dataArr};
	var compiled = Handlebars.compile(hbTemplate)(context);
	$('#' + holder).html(compiled);

	if (currentPage == 1) {
		$('#' + paginator + ' ._pag__prev,' + '#' + paginator + ' ._pag__first').attr('disabled', 'disabled');
	}
	else {
		$('#' + paginator + ' ._pag__prev,' + '#' + paginator + ' ._pag__first').removeAttr('disabled');
	}
	if (to >= data.total) {
		$('#' + paginator + ' ._pag__next,' + '#' + paginator + ' ._pag__last').attr('disabled', 'disabled');
	}
	else {
		$('#' + paginator + ' ._pag__next,' + '#' + paginator + ' ._pag__last').removeAttr('disabled');
	}

	$('#' + paginator + ' ._pag__first').off().click(function() {
		paginateAjaxForTemplate(null, 1, paginator, template, holder);
	});
	$('#' + paginator + ' ._pag__last').off().click(function() {
		paginateAjaxForTemplate(null, data.last_page, paginator, template, holder);
	});
	$('#' + paginator + ' ._pag__prev').off().click(function() {
		paginateAjaxForTemplate(null, (data.current_page - 1), paginator, template, holder);
	});
	$('#' + paginator + ' ._pag__next').off().click(function() {
		paginateAjaxForTemplate(null, (data.current_page + 1), paginator, template, holder);
	});


	setupTemplatePaginationActions(page, paginator, template, holder);
}

function setupTemplatePaginationActions(page, paginator, template, holder) {
	if (paginator == 'user-absences-pag') {
		$('._delete-absence').off().click(function(e) {
			e.preventDefault();
			if ($('#user-absences-pag').attr('data-absence-type') == 'sick_leaves') {
				deleteAbsence('sick_leaves', $(this).attr('data-absence'), function(data) {
					paginateAjaxForTemplate(null, page, paginator, template, holder);
				});
			}
			else {
				deleteAbsence('vacations', $(this).attr('data-absence'), function(data) {
					paginateAjaxForTemplate(null, page, paginator, template, holder);
				});
			}
		});
	}
	if (paginator == 'user-check-ins-pag') {
		$('._delete-check-in').off().click(function(e) {
			e.preventDefault();
			deleteCheckIn($(this).attr('data-check-in'), function(data) {
				paginateAjaxForTemplate(null, page, paginator, template, holder);
			});
		});
	}

	if (paginator == 'date-check-ins-pag') {
		$('._delete-check-in').off().click(function(e) {
			e.preventDefault();
			deleteCheckIn($(this).attr('data-check-in'), function(data) {
				paginateAjaxForTemplate(null, page, paginator, template, holder);
			});
		});
	}
}

function deleteAbsence(type, id, callback) {
	showDefaultConfirmSwal(null, 'Odabrana stavka bit će obrisana.', null, null, function() {    
        $.post(APP_URL + '/admin/ajax/absence/' + type + '/' + id + '/destroy', function(r) {}, "json")
	        .done(function(data) {
	            callback(data);
	        })
	        .fail(function (data) {
	        });
    });
}

function deleteCheckIn(id, callback) {
	showDefaultConfirmSwal(null, 'Prijava će biti u potpunosti obrisana.', null, null, function() {  
        $.post(APP_URL + '/admin/ajax/check-in/' + id + '/destroy', function(r) {}, "json")
	        .done(function(data) {
	            callback(data);
	        })
	        .fail(function (data) {
	        });
    });
}

function stopCurrentCheckIn(id, callback) {
	showDefaultConfirmSwal(null, 'Prijava će biti zaustavljena na trenutno vrijeme.', null, null, function() {
		$.post(APP_URL + '/admin/ajax/check-in/' + id + '/stop', function(r) {}, "json")
	        .done(function(data) {
	            callback(data);
	        })
	        .fail(function (data) {
	        });
	});
}

function openCheckInsDatePicker() {
	$('#modal-checkins-date-picker').modal('show');
	$('#checkins-date-picker').datetimepicker({
        inline: true,
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });
    $('#checkins-date-picker').off('dp.change').on('dp.change', function(event) {
    	$('#modal-checkins-date-picker').modal('hide');
    	var selectedDate = event.date;
    	$('#date-check-ins-pag').attr('data-date', selectedDate.format('YYYY-MM-DD'));
	    $('#dashboard-date').html(event.date.format('DD.MM.YYYY.'));
    	if (selectedDate.isSame(moment(), 'day')) {
	    	$('#checkins-custom-date-row').slideUp();
	    	$('#checkins-today-row').slideDown();
	    	drawCurrentSectionsProgress();
    	}
    	else {
			paginateAjaxForTemplate(null, null, 'date-check-ins-pag', 'check-ins-custom-date-hbt', 'check-ins-date-holder');
	    	$('#checkins-today-row').slideUp();
	    	$('#checkins-custom-date-row').slideDown();
	    }
    });
}

function makeActivityChartByHour(data) {
	hours = [];
	for(var i = 0 ; i < 24 ; i++) {
		hours.push({hour: moment("00:00", "HH:mm").add(i, 'hours'), data: []});
	}
	console.log('hours', hours);
	_.each(hours, function(value, i, obj) {
		console.log('obj', obj);
		obj[i].data = _.filter(data, function(item) {
			var dataDate = moment(item.check_in_local.date).format('YYYY-MM-DD');
			var objMoment = moment(dataDate + ' ' + obj[i].hour.format('HH:mm'), 'YYYY-MM-DD HH:mm');
			return ((moment(item.check_in_local.date).add(1, 'h').isBefore(objMoment) || moment(item.check_in_local.date).add(1, 'h').isSame(objMoment, 'hour')) && (moment(item.check_out_local.date).isAfter(objMoment)));
		});
	});
	console.log('hours', hours);

	var onlyHours = _.pluck(hours, 'hour');
	var onlyHoursFormatted = _.map(onlyHours, function (value, key) {
		console.log(value.format('HH'));
    	return value.format('HH') + "h";
    });
	var onlyData = _.pluck(hours, 'data');
	var onlyDataNumeric = _.map(onlyData, function (value, key) {
    	return value.length;
    });

	$('#dashboard-date-chart').remove();
  	$('#dashboard-date-chart-holder').append('<canvas id="dashboard-date-chart" style="width: 100%; height: 300px"></canvas>');
	var ctx = $('#dashboard-date-chart');
	var lineChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	    	labels: onlyHoursFormatted,
		    datasets: [{
	            label: "Prijave kroz dan",
	            fill: true,
	            backgroundColor: "rgba(75,192,192,0.4)",
	            borderColor: "rgba(75,192,192,1)",
	            pointBorderColor: "rgba(75,192,192,1)",
	            pointBackgroundColor: "#fff",
	            pointHoverBackgroundColor: "rgba(75,192,192,1)",
	            pointHoverBorderColor: "rgba(220,220,220,1)",
	            data: onlyDataNumeric,
	            lineTension: 0
		    }]
		},
		options: {
			legend: {
            	display: false
        	}
     	}
	});
}