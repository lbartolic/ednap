import Echo from "laravel-echo";

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: 'http://localhost:6001'
});

window.echoListenCheckIn = function() {
	window.Echo.channel('check-in')
		.listen('NewCheckInEvent', function(data) {
		    console.log(data);
			appendNewCheckIn(data);
		});
}
window.echoListenCheckOut = function() {
	window.Echo.channel('check-out')
		.listen('NewCheckOutEvent', function(data) {
			console.info(data);
			removeCurrentCheckIn(data);
			/*$('#chkins-tbl-body').find('[data-user="' + data.check_in.user.id + '"]')
				.css({'background' : 'rgba(214, 89, 89, 0.3)'})
				.delay(130)
				.fadeOut('fast');
		    var template = $('#new-not-checked-in-hbt').html();
		    var compiled = Handlebars.compile(template)(data.check_in);
		    $('#not-chkdin-tbl-body').append(compiled);*/
		});
}