<?php namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\CheckIn\CheckIn;
use App\Models\User\User;
use App\Models\Sector\Sector;

use Auth;
use Config;
use Carbon\Carbon;

class SectorRepository {
    public function totalCount() {
        return Sector::count();
    }

    public function getPaginated(Request $request) {
    	$sectors = Sector::with('users')->withCount('users');

        switch($request->input('order_by')) {
            case 'emp_count_asc':
                $sectors = $sectors->orderBy('users_count', 'ASC');
                break;
            case 'emp_count_desc':
                $sectors = $sectors->orderBy('users_count', 'DESC');
                break;
            case 'name_asc':
                $sectors = $sectors->orderBy('name', 'ASC');
                break;
            case 'name_desc':
                $sectors = $sectors->orderBy('name', 'DESC');
                break;
            default:
            	$sectors = $sectors->orderBy('name', 'ASC');
            	break;
        }

        $searchTerm = null;
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $sectors = $sectors->where('name', 'LIKE', '%'. $searchTerm .'%')->orWhere('abbrevation', 'LIKE', '%'. $searchTerm .'%');
        }

        $sectors = $sectors->paginate(5);

        return [
            'sectors' => $sectors,
            'request_params' => array_except($request->all(), ['page']),
            'search_term' => $searchTerm
        ];
    }
}