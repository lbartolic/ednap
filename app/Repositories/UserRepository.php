<?php namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\CheckIn\CheckIn;
use App\Models\User\User;

use Auth;
use Config;
use Carbon\Carbon;

class UserRepository {
	public function notCheckedIn($filters = null) {
        return User::with('sectors')
            ->notCheckedIn()
            ->get();
    }

    public function totalCount() {
    	return User::count();
    }

    public function onVacationCount() {
    	return User::onVacation()->count();
    }

    public function onSickLeaveCount() {
    	return User::onSickLeave()->count();
    }

    public function getPaginated(Request $request) {
    	//$users = User::with('sectors', 'checkIns');

    	$users = User::withTotalWorkingTime()->with('sectors', 'checkIns', 'vacations', 'sickLeaves');

    	switch($request->input('absence')) {
        	case 'vacations':
            	$users = $users->onVacation();
            	break;
            case 'sick-leaves':
            	$users = $users->onSickLeave();
            	break;
        }
        switch($request->input('order_by')) {
            case 'last_name_asc':
                $users = $users->orderBy('last_name', 'ASC');
                break;
            case 'last_name_desc':
                $users = $users->orderBy('last_name', 'DESC');
                break;
            case 'created_at_asc':
                $users = $users->orderBy('created_at', 'ASC');
                break;
            case 'created_at_desc':
                $users = $users->orderBy('created_at', 'DESC');
                break;
            case 'working_time_asc':
            	$users = $users->orderBy('working_time', 'ASC');
            	break;
            case 'working_time_desc':
            	$users = $users->orderBy('working_time', 'DESC');
            	break;
            default:
            	$users = $users->orderBy('last_name', 'ASC');
            	break;
        }

        $searchTerm = null;
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $users = $users->where('first_name', 'LIKE', '%'. $searchTerm .'%')
                ->orWhere('last_name', 'LIKE', '%'. $searchTerm .'%')
                ->orWhere('email', 'LIKE', '%'. $searchTerm .'%');
        }

        $users = $users->paginate(5);

        return [
            'users' => $users,
            'request_params' => array_except($request->all(), ['page']),
            'search_term' => $searchTerm
        ];
    }

    public function generateUsersForList($users) {
        $users = $users->map(function($user) {
            $user['list_param'] = $user->first_name . ' ' . $user->last_name;
            return $user;
        });
        return $users->pluck('list_param', 'id');
    }
}