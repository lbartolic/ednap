<?php namespace App\Repositories;

use Illuminate\Http\Request;

use App\Models\CheckIn\CheckIn;
use App\Models\User\User;

use Auth;
use Config;
use Carbon\Carbon;

class CheckInRepository {

	public function current($filters = null) {
        $current = CheckIn::with('user', 'user.sectors')
            ->currentlyCheckedIn()
            ->orderBy('check_in', 'DESC')
            ->get();

        return $current;
    }

    public function forDatePaginated($date) {
    	return $this->forDate($date)->paginate(10);
    }

    public function statsForDate($date) {
    	return $this->forDate($date)->get();
    }

    public function forDate($date) {
    	$checkIns = CheckIn::with('user', 'user.sectors')
            ->whereDate('check_in', '>=', Carbon::createFromFormat('Y-m-d', $date)->startOfDay())
            ->whereDate('check_in', '<=', Carbon::createFromFormat('Y-m-d', $date)->endOfDay())
            ->orderBy('check_in', 'ASC');

        return $checkIns;
    }

    /**
     * Return true if possible to stop
     * Return false if not possible to stop (already checked out)
     */
    public function stopCheckIn(CheckIn $checkIn) {
    	if ($checkIn->check_out == NULL) {
    		$checkIn->update(['check_out' => Carbon::now()]);
    		return true;
    	}
    	return false;
    }
}