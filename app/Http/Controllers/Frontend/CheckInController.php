<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\CheckIn\CheckIn;

use Carbon\Carbon;

class CheckInController extends Controller
{
    public function postMake(Request $request) {
        $user = Auth::user();

        if (!$user->is_checked_in) {
            $checkIn = $user->checkIns()->create([
                'check_in' => Carbon::now(),
                'tz' => 'Europe/Zagreb'
            ]);

            event(new \App\Events\NewCheckInEvent($checkIn));
        }
        return redirect()->back();
    }

    public function postCheckOut(Request $request) {
        $user = Auth::user();

        if ($user->is_checked_in) {
            $checkIn = $user->checkIns()->whereNull('check_out')->first();
            $checkIn->update(['check_out' => Carbon::now()]);

            event(new \App\Events\NewCheckOutEvent($checkIn));
        }
        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
