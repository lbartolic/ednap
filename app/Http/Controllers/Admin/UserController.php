<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;

use App\Models\Sector\Sector;
use App\Models\User\User;

class UserController extends Controller
{
    public function __construct(UserRepository $users) {
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usersData = $this->users->getPaginated($request);
        $allUsers = User::all();
        $counts = [
            'total' => $this->users->totalCount(),
            'vacations' => $this->users->onVacationCount(),
            'sick_leaves' => $this->users->onSickLeaveCount()
        ];

        return view('admin.layouts.users.index', [
            'users' => $usersData['users'],
            'requestParams' => $usersData['request_params'],
            'searchTerm' => $usersData['search_term'],
            'counts' => $counts,
            'allUsers' => $allUsers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sectors = Sector::pluck('name', 'id');
        return view('admin.layouts.users.create', [
            'sectors' => $sectors,
            'selected_sectors' => null
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());

        $sectors = [];
        if ($request->has('sectors')) $sectors = $request->input('sectors');
        $user->sectors()->sync($sectors);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::withTotalWorkingTime()->with('sectors', 'checkIns')
            ->where('users.id', $id)->first();

        return view('admin.layouts.users.show', [
            'user' => $user
        ]);
    }

    public function getCheckIns(Request $request, $id) {
        if ($request->ajax()) {
            $user = User::where('id', $id)->first();
            $checkIns = $user->checkIns()->orderBy('check_in', 'DESC')->paginate(5);
            return response()->json($checkIns);
        }
    }

    public function getVacations(Request $request, $id) {
        if ($request->ajax()) {
            $user = User::where('id', $id)->first();
            $vacations = $user->vacations()->orderBy('date_from', 'DESC')->paginate(5);
            return response()->json($vacations);
        }
    }

    public function getSickLeaves(Request $request, $id) {
        if ($request->ajax()) {
            $user = User::where('id', $id)->first();
            $sickLeaves = $user->sickLeaves()->orderBy('date_from', 'DESC')->paginate(5);
            return response()->json($sickLeaves);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $sectors = Sector::pluck('name', 'id');
        $selectedSectors = $user->sectors->pluck('id')->toArray();
        return view('admin.layouts.users.edit', [
            'sectors' => $sectors,
            'selected_sectors' => $selectedSectors,
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $requestData = $request->all();
        if ($requestData['password'] == '' || $requestData['password'] == null) {
            $requestData = $request->except(['password']);
        }
        $user->update($requestData);

        $sectors = [];
        if ($request->has('sectors')) $sectors = $request->input('sectors');
        $user->sectors()->sync($sectors);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();

        return redirect()->back();
    }
}
