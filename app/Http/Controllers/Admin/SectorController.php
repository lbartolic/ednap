<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\SectorRepository;
use App\Repositories\UserRepository;


use App\Models\Sector\Sector;
use App\Models\User\User;

class SectorController extends Controller
{
    public function __construct(SectorRepository $sectors, UserRepository $users) {
        $this->sectors = $sectors;
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sectorsData = $this->sectors->getPaginated($request);
        $users = $this->users->generateUsersForList(User::all());
        $counts = [
            'total' => $this->sectors->totalCount()
        ];

        return view('admin.layouts.sectors.index', [
            'sectors' => $sectorsData['sectors'],
            'requestParams' => $sectorsData['request_params'],
            'searchTerm' => $sectorsData['search_term'],
            'users' => $users,
            'counts' => $counts,
            'selected_users' => null
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$sectors = Sector::all();
        return view('admin.layouts.sectors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sector = Sector::create($request->all());

        $users = [];
        if ($request->has('users')) $users = $request->input('users');
        $sector->users()->sync($users);
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::where('id', $id)->first();
        $users = $this->users->generateUsersForList(User::all());
        $selectedUsers = $sector->users->pluck('id')->toArray();
        return view('admin.layouts.sectors.edit', [
            'sector' => $sector,
            'users' => $users,
            'selected_users' => $selectedUsers
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sector = Sector::where('id', $id)->first();
        $sector->update($request->all());

        $users = [];
        if ($request->has('users')) $users = $request->input('users');
        $sector->users()->sync($users);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sector = Sector::where('id', $id)->first();
        $sector->delete();

        return redirect()->back();
    }
}
