<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\CheckInRepository;
use App\Repositories\UserRepository;

use App\Models\CheckIn\CheckIn;
use App\Models\User\User;

use Carbon\Carbon;

class CheckInController extends Controller
{
    public function __construct(CheckInRepository $checkIns, UserRepository $users) {
        $this->checkIns = $checkIns;
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkIn = new CheckIn();
        $checkIn->tz = $request->input('tz');

        if ($request->input('checkin_type') == 'current') {
            $checkInFrom = Carbon::createFromFormat('H:i', $request->input('check_in'), $checkIn->tz)
                ->tz('UTC');
            $checkInTo = NULL;
        }
        if ($request->input('checkin_type') == 'past') {
            $checkInFrom = Carbon::createFromFormat('d.m.Y. H:i', $request->input('check_in'), $checkIn->tz)
                ->tz('UTC');
            $checkInTo = Carbon::createFromFormat('d.m.Y. H:i', $request->input('check_in'), $checkIn->tz)
                ->tz('UTC')->addMinutes($request->input('check_in_duration')*60);
        }

        $user = User::where('id', $request->input('user'))->first();
        $checkIn->check_in = $checkInFrom;
        $checkIn->check_out = $checkInTo;
        $user->checkIns()->save($checkIn);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getEditCheckIn(Request $request, $id) {
        if ($request->ajax()) {
            $checkIn = CheckIn::with('user')->where('id', $id)->first();

            return response()->json($checkIn);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCheckedIn(Request $request) {
        if ($request->ajax()) {
            $current = $this->checkIns->current();

            return response()->json($current);
        }
    }

    public function getCheckedInForDate(Request $request, $date) {
        if ($request->ajax()) {
            $checkIns = $this->checkIns->forDatePaginated($date);

            return response()->json($checkIns);
        }
    }

    public function postDestroy(Request $request, $id) {
        if ($request->ajax()) {
            $checkIn = CheckIn::where('id', $id)->first();
            event(new \App\Events\NewCheckOutEvent($checkIn));
            $checkIn->delete($id);
            return response()->json($checkIn);
        }
    }

    public function postStop(Request $request, $id) {
        if ($request->ajax()) {
            $checkIn = CheckIn::where('id', $id)->first();
            $result = $this->checkIns->stopCheckIn($checkIn);
            event(new \App\Events\NewCheckOutEvent($checkIn));
            return response()->json($result);
        }
    }

    public function getStatsForDate(Request $request, $date) {
        if ($request->ajax()) {
            $checkIns = $this->checkIns->statsForDate($date);
            return response()->json($checkIns);
        }
    }
}
