<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;

use App\Models\Vacation\Vacation;
use App\Models\SickLeave\SickLeave;
use App\Models\User\User;

class AbsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->input('absence_type');
        $user = User::where('id', $request->input('user'))->first();

        if ($type == 'sick_leave') {
            $user->sickLeaves()->create($request->all());
        }
        if ($type == 'vacation') {
            $user->vacations()->create($request->all());
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postDestroy(Request $request, $type, $id) {
        if ($request->ajax()) {
            if ($type == 'sick_leaves') {
                $absence = SickLeave::where('id', $id)->first();
            }
            else {
                $absence = Vacation::where('id', $id)->first();
            }
            $absence = $absence->delete($id);
            return response()->json($absence);
        }
    }
}
