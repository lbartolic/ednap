<?php

namespace App\Models\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', "is_admin"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $appends = [
        'full_name'
    ];

    public function sectors() {
        return $this->belongsToMany('App\Models\Sector\Sector', 'user_sector')->withTimestamps();
    }

    public function checkIns() {
        return $this->hasMany('App\Models\CheckIn\CheckIn');
    }

    public function vacations() {
        return $this->hasMany('App\Models\Vacation\Vacation');
    }

    public function sickLeaves() {
        return $this->hasMany('App\Models\SickLeave\SickLeave');
    }

    public function scopeNotCheckedIn($query) {
        return $query->whereDoesntHave('checkIns', function($q) {
                $q->whereNull('check_out');
            })
            ->doesntHave('checkIns', 'or');
    }

    public function scopeWithTotalWorkingTime($query, $dateStart = null, $dateEnd = null) {
        $workingTime = $query->leftJoin('check_ins AS ci', 'ci.user_id', '=', 'users.id')
            ->select(DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(ci.check_out, ci.check_in))) AS working_time'))
            ->groupBy('users.id')
            ->addSelect('users.*');
        /*if ($dateStart != null && $dateEnd != null) {
            $workingTime = $workingTime->whereHas('checkIns', function($query) {
                $query->whereDate('check_in', '>=', Carbon::createFromFormat('Y-m-d', $dateStart)->startOfDay()->toDateTimeString())
                    ->whereDate('check_in', '<=', Carbon::createFromFormat('Y-m-d', $dateEnd)->endOfDay()->toDateTimeString());
            });
        }*/
        return $workingTime;
    }

    public function scopeOnVacation($query) {
        return $query->whereHas('vacations', function($query) {
            $query->current();
        });
    }

    public function scopeOnSickLeave($query) {
        return $query->whereHas('sickLeaves', function($query) {
            $query->current();
        });
    }

    public function getCurrentVacationAttribute() {
        return $this->vacations->where('date_from', '<=', Carbon::today())
            ->where('date_to', '>=', Carbon::today())->first();
    }

    public function getCurrentSickLeaveAttribute() {
        return $this->sickLeaves->where('date_from', '<=', Carbon::today())
            ->where('date_to', '>=', Carbon::today())->first();
    }

    public function getAdminCheckAttribute() {
        return ($this->is_admin == 1) ? true : false;
    }

    public function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getIsCheckedInAttribute() {
        $count = $this->checkIns()->whereNull('check_out')->count();
        return ($count > 0) ? true : false;
    }

    public function getTotalWorkingTimeHiAttribute() {
        $hours = floor($this->working_time / 3600);
        $mins = floor($this->working_time / 60 % 60);
        return sprintf('%02d:%02d', $hours, $mins);
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }
}
