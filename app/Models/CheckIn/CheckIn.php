<?php

namespace App\Models\CheckIn;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CheckIn extends Model
{
	protected $fillable = [
		'check_in', 'check_out', 'tz'
	];

	protected $dates = [
		'check_in', 'check_out'
	];

	protected $appends = [
        'check_in_local', 'check_out_local', 'check_in_diff', 'check_in_local_time_format',
        'check_out_local_time_format'
    ];
    
    public function user() {
		return $this->belongsTo('App\Models\User\User');
	}

    public function scopeCurrentlyCheckedIn($query) {
        return $query->whereNull('check_out');
    }

    public function scopeWithTest($query) {
    $query->addSelect(\DB::raw('SUM(TIME_TO_SEC(check_out) - TIME_TO_SEC(check_in)) AS timediff'));
}

	public function getCheckInLocalAttribute() {
		return $this->check_in->timezone($this->tz);
	}

	public function getCheckInLocalTimeFormatAttribute() {
		return $this->check_in_local->format('H:i');
	}

    public function getCheckOutLocalTimeFormatAttribute() {
        return ($this->check_out == null) ? null : $this->check_out_local->format('H:i');
    }

	public function getCheckOutLocalAttribute() {
		return ($this->check_out == null) ? null : $this->check_out->timezone($this->tz);
	}

	public function getCheckInDiffAttribute() {
		if ($this->check_out) return gmdate('H:i', $this->check_out->diffInSeconds($this->check_in));
		return gmdate('H:i', Carbon::now()->diffInSeconds($this->check_in));
	}
}
