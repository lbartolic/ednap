<?php

namespace App\Models\Sector;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name', 'abbrevation'
	];

    public function users() {
		return $this->belongsToMany('App\Models\User\User', 'user_sector')->withTimestamps();
	}
}
