<?php

namespace App\Models\Vacation;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Vacation extends Model
{
	protected $fillable = [
		'date_from', 'date_to'
	];
	protected $dates = [
		'date_from', 'date_to'
	];

    public function user() {
		return $this->belongsTo('App\Models\User\User');
	}
	
	public function getDateFromDiffDaysAttribute() {
		return $this->date_from->diffInDays(Carbon::today());
	}

	public function scopeCurrent($query) {
		return $query->where('date_from', '<=', Carbon::today())
            ->where('date_to', '>=', Carbon::today());
	}

	public function setDateFromAttribute($value) {
		$this->attributes['date_from'] = Carbon::createFromFormat('d.m.Y.', $value)->startOfDay();
	}

	public function setDateToAttribute($value) {
		$this->attributes['date_to'] = Carbon::createFromFormat('d.m.Y.', $value)->startOfDay();
	}
}
