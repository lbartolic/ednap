<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\CheckIn\CheckIn;
use App\Models\User\User;

class NewCheckInEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $checkIn;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CheckIn $checkIn)
    {
        $this->checkIn = $checkIn;
    }

    public function broadcastWith() {
        $checkIn = CheckIn::with('user', 'user.sectors')->where('id', $this->checkIn->id)->first();
        
        return ['check_in' => $checkIn];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['check-in'];
    }
}
