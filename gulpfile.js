var elixir = require('laravel-elixir');

elixir(function(mix) {
	/**
	 * Copy files
	 */
	mix.copy(
		'resources/assets/fonts',
		'public/fonts'
	);
	
	 /**
	  * Admin SASS
	  */
	mix.sass([
		"./resources/assets/sass/login.scss",
		"./resources/assets/sass/admin.scss"
	], "./resources/assets/sass/app_compiled/admin.css");

	 /**
	  * Frontend SASS
	  */
	mix.sass([
		"./resources/assets/sass/frontend.scss"
	], "./resources/assets/sass/app_compiled/frontend.css");

	 /**
	  * Admin styles
	  */
	mix.styles([
	    "./resources/assets/css/animate.css",
		"./resources/assets/css/bootstrap-flatly.min.css",
		"./resources/assets/css/bootstrap-datetimepicker.min.css",
		"./resources/assets/css/formvalidatorTheme.min.css",
		"./resources/assets/css/select2.min.css",
		"./resources/assets/css/font-awesome.min.css",
		"./resources/assets/css/sweetalert.css",
		"./resources/assets/css/lobibox.min.css",
		"./resources/assets/sass/app_compiled/admin.css"
	], 'public/css/admin.css');

	 /**
	  * Frontend styles
	  */
	mix.styles([
		"./resources/assets/css/bootstrap-flatly.min.css",
		"./resources/assets/css/select2.min.css",
		"./resources/assets/css/font-awesome.min.css",
		"./resources/assets/css/sweetalert.css",
		"./resources/assets/css/lobibox.min.css",
		"./resources/assets/sass/app_compiled/frontend.css"
	], 'public/css/frontend.css');  
	 

	 /**
	  * Admin scripts
	  */
	mix.browserify([
		'./resources/assets/js/app/echo.js'
	], './resources/assets/js/app/app_compiled/echo.js');
	mix.scripts([
	    "./resources/assets/js/jquery-1.10.2.js",
		"./resources/assets/js/jquery-ui.js",
		"./resources/assets/js/bootstrap.min.js",
		"./resources/assets/js/moment.js",
		"./resources/assets/js/bootstrap-datetimepicker.min.js",
		"./resources/assets/js/select2.full.min.js",
		"./resources/assets/js/chartjs.min.js",
		"./resources/assets/js/jquery.form-validator.min.js",
		"./resources/assets/js/underscore.min.js",
		"./resources/assets/js/handlebars-v4.0.5.js",
		"./resources/assets/js/handlebars-intl.min.js",
		"./resources/assets/js/hr.js",
		"./resources/assets/js/sweetalert.min.js",
		"./resources/assets/js/nanoscroller.min.js",
		"./resources/assets/js/lobibox.min.js",
		"./resources/assets/js/app/handlebars-helpers.js",
		"./resources/assets/js/app/charts.js",
		"./resources/assets/js/app/app_compiled/echo.js",
		"./resources/assets/js/app/admin.js"
	], 'public/js/admin.js');


	 /**
	  * Frontend scripts
	  */



	mix.version([
		"public/js/admin.js"
	]);
});