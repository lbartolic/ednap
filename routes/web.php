<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'AppController@index');

	Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin', 'namespace' => 'Admin'], function() {
		Route::get('/', [
			'uses' => 'AdminController@index',
			'as' => 'index',
		]);

		Route::resource('user', 'UserController');
		Route::resource('check-in', 'CheckInController');
		Route::resource('sector', 'SectorController');
		Route::resource('absence', 'AbsenceController');

		Route::group(['prefix' => 'ajax'], function() {
			Route::get('check-in/check-ins/{date}', 'CheckInController@getCheckedInForDate');
			Route::get('check-in/check-ins/{date}/stats', 'CheckInController@getStatsForDate');
			Route::get('check-in/get-checked-in', 'CheckInController@getCheckedIn');
			Route::get('check-in/{check_in}/edit', 'CheckInController@getEditCheckIn');
			Route::post('check-in/{check_in_id}/destroy', 'CheckInController@postDestroy');
			Route::post('check-in/{check_in_id}/stop', 'CheckInController@postStop');

			Route::get('user/{user_id}/check-ins', 'UserController@getCheckIns');
			Route::get('user/{user_id}/absence/vacations', 'UserController@getVacations');
			Route::get('user/{user_id}/absence/sick-leaves', 'UserController@getSickLeaves');

			Route::post('absence/{type}/{absence_id}/destroy', 'AbsenceController@postDestroy');
		});
	});

	Route::group(['prefix' => 'employee', 'namespace' => 'Frontend'], function() {
		Route::get('/', [
			'uses' => 'FrontendController@index',
			'as' => 'frontend.index',
		]);

		Route::post('check-in/make', [
			'uses' => 'CheckInController@postMake',
			'as' => 'frontend.check-in.make'
		]);
		Route::post('check-in/check-out', [
			'uses' => 'CheckInController@postCheckOut',
			'as' => 'frontend.check-in.check-out'
		]);
		Route::resource('check-in', 'CheckInController');
	});
});

Auth::routes();

Route::get('/logout', function() {
	\Auth::logout();
	return redirect('/');
});