var echo = require('laravel-echo-server');

var options = {
  authHost: 'http://localhost',
  authPath: '/broadcasting/auth',
  host: 'localhost',
  port: 6001
};

echo.run(options);
